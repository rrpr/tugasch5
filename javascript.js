const computerChoiceDisplay = document.getElementById('computer-choice')
const userChoiceDisplay = document.getElementById('user-choice')
const resultDisplay = document.getElementById('result')
const possibleChoices = document.querySelectorAll('button')
let userChoice
let computerChoice
let result

possibleChoices.forEach(possibleChoice => possibleChoice.addEventListener('click', (e) => {
  userChoice = e.target.id
  userChoiceDisplay.innerHTML = userChoice
  generateComputerChoice()
  getResult()
}))


function generateComputerChoice() {
  const randomNumber = Math.floor(Math.random() * 3) + 1 // or you can use possibleChoices.length
  
  if (randomNumber === 1) {
    computerChoice = 'Rock'
  }
  if (randomNumber === 2) {
    computerChoice = 'Scissors'
  }
  if (randomNumber === 3) {
    computerChoice = 'Paper'
  }
  computerChoiceDisplay.innerHTML = computerChoice
}

function getResult() {
  if (computerChoice === 'Rock' && userChoice === "rock") {
    result = 'Seri!'
  }
  if (computerChoice === 'Paper' && userChoice === "paper") {
    result = 'Seri'
  }
  if (computerChoice === 'Scissors' && userChoice === "scissors") {
    result = 'Seri'
  }
  if (computerChoice === 'Rock' && userChoice === "paper") {
    result = 'Menang!'
  }
  if (computerChoice === 'Rock' && userChoice === "scissors") {
    result = 'Kalah!'
  }
  if (computerChoice === 'Paper' && userChoice === "scissors") {
    result = 'Menang!'
  }
  if (computerChoice === 'Paper' && userChoice === "rock") {
    result = 'Kalah!'
  }
  if (computerChoice === 'Scissors' && userChoice === "rock") {
    result = 'Menang!'
  }
  if (computerChoice === 'Scissors' && userChoice === "paper") {
    result = 'Kalah!'
  }
  resultDisplay.innerHTML = result
  function clearResult(){
    document.getElementById("result").value = ''
  }

  
}