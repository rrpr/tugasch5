const express = require('express')
const app = express()
const path = require('path')
const route = express.Router()
const cors = require('cors')
const bodyParser = require('body-parser');
const errorHandler = require('./_helpers/error-handler');
app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use("/assets", express.static(path.join(__dirname, "assets")));
app.use(express.static(__dirname));
app.use(express.json({ limit: "100mb" }));

// api routes
app.use('/users', require('./users/users.controller'));

// global error handler
app.use(errorHandler);



app.get("/", function(req, res){
    res.render(__dirname + "/index.ejs")
});

app.get("/index", function(req, res){
    res.render(__dirname + "/index.ejs")
});

app.get("/aboutme", function(req, res){
    res.render(__dirname + "/aboutme.ejs")
});

app.get("/work", function(req, res){
    res.render(__dirname + "/work.ejs")
});

app.get("/contact", function(req, res){
    res.render(__dirname + "/contact.ejs")
});

app.get("/signup", function(req, res){
    res.render(__dirname + "/signup.ejs")
});

app.get("/login", function(req, res){
    res.render(__dirname + "/login")
});

app.get("/game", function(req, res){
    res.render(__dirname + "/game")
});



app.listen(3000, function() {
    console.log("Server  started on port 3000")
});