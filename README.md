1. Pindahkan code challenge pada chapter 3 dan 4
yang semula merupakan HTML statis ke dalam
server menggunakan Express, halaman 1
dengan yang lainnya dibedakan dengan routing

{ ada di localhost:3000 }

2. Membuat data user static untuk login di bagian
backend 

{ post request localhost:3000/users/authenticate dalam bentuk raw: JSON
{
    "username": "test",
    "password": "test"
}

3. Menggunakan Postman untuk mengecek API
{ ok oc }

4. Serving data user static ke bentuk JSON

{ buat get request ke localhost:3000/users/
input : Authorization
input type : Basic Auth

{
    "username": "test",
    "password": "test"
}
}